# java-integration-j2v8-example

Example project demonstrating integration of the mortgage model with Java using the J2V8 bindings

## Contents

A simple maven project.

src/main/java: 
 - the MortgageCalculations.java wrapper for J2V8
 - a __main__.java calling the calculations
 
src/main/resources: Js/mortgageCalculations.js is the generated JavaScript code.

## Using the project

```bash
mvn clean install
mvn exec:java -D"exec.mainClass"="formulator.bundle.__main__"
``` 
