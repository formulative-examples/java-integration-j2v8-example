package formulator.bundle;

import com.eclipsesource.v8.V8Object;
import com.google.gson.JsonObject;

/**
 * Ezt nem kell legyártani, csak a minta hívás miatt írtam meg...
 * @author mob
 *
 */
public class __main__ {

    public static void main(String[] args) {
        try {
            // initialize resources, load Js bundle from fs
            MortgageCalculations mortgageCalculations = new MortgageCalculations();

            // setup parameters
            JsonObject params = new JsonObject();
            params.addProperty(MortgageCalculations.MORTGAGE_PMT_FROM_SCORE__PMT__CLIENT_SCORE, "620");
            params.addProperty(MortgageCalculations.MORTGAGE_PMT_FROM_SCORE__PMT__TERM, "30");
            params.addProperty(MortgageCalculations.MORTGAGE_PMT_FROM_SCORE__PMT__PRINCIPAL, "200000");

            // call calculation
            V8Object result = mortgageCalculations.doCalculation(
                    MortgageCalculations.MORTGAGE_CALCULATIONS__MORTGAGE_PMT_FROM_SCORE,
                    MortgageCalculations.MORTGAGE_PMT_FROM_SCORE__PMT,
                    params);

            // print result
            System.out.println("monthlyPayment = " + result.get(
                    MortgageCalculations.MORTGAGE_PMT_FROM_SCORE__PMT__MONTHLY_PAYMENT));

            // you have to release j2v8 V8Object object yourself if you asked for it
            result.release();

            // if you prefer the result as JsonObject call this
            JsonObject resultAsJsonObj = mortgageCalculations.doCalculationAsJsonObject(
                    MortgageCalculations.MORTGAGE_CALCULATIONS__MORTGAGE_PMT_FROM_SCORE,
                    MortgageCalculations.MORTGAGE_PMT_FROM_SCORE__PMT,
                    params);

            // print result
            System.out.println("result as a JSON object: " + resultAsJsonObj);

            // call close() before releasing "calc" object
            mortgageCalculations.close();
            mortgageCalculations = null;
        } catch(Exception ex) {
            System.out.println(ex);
        }
    }

}