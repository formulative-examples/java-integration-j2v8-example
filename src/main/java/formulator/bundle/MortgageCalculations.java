package formulator.bundle;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.eclipsesource.v8.V8;
import com.eclipsesource.v8.V8Array;
import com.eclipsesource.v8.V8Object;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MortgageCalculations {

	// Service constants
	public static String MORTGAGE_CALCULATIONS__MORTGAGE_PMT_FROM_SCORE = "mortgagePmtFromScore";

	// Calculation constants
	public static String MORTGAGE_PMT_FROM_SCORE__PMT = "pmt";

	// Variable constants
	public static String MORTGAGE_PMT_FROM_SCORE__PMT__CLIENT_SCORE = "clientScore";
	public static String MORTGAGE_PMT_FROM_SCORE__PMT__TERM = "term";
	public static String MORTGAGE_PMT_FROM_SCORE__PMT__PRINCIPAL = "principal";
	public static String MORTGAGE_PMT_FROM_SCORE__PMT__MONTHLY_PAYMENT = "monthlyPayment";

	private V8 runtime;
	private Gson gson;

	/**
	 * Constructor initializes JS engine and GSON instances. Can throw exception if
	 * Js bundle is not found.
	 * @throws Exception
	 */
	public MortgageCalculations() throws Exception	{
		super();
		try {
			String bundleSource = readFileFromClasspath("Js/mortgageCalculations.js");
			runtime = V8.createV8Runtime();
			runtime.executeScript("" + "var GLOBAL={};" + "var LOCALEID=\"en_US\";");
			runtime.executeScript(bundleSource);
			gson = new GsonBuilder().serializeNulls().create();
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * Call close if you will not need this instance anymore
	 */
	public void close() {
		this.runtime.release();
		this.gson = null;
	}

	/**
	 * Call doCalculation to perform a calculation and return the result as V8Object
	 * @param service - name of service
	 * @param calculation - name of calculation
	 * @param params - JSON parameters object
	 * @return - V8Object (for details see: https://github.com/eclipsesource/J2V8)
	 */
	public V8Object doCalculation(String service, String calculation, JsonObject params) {
		V8Object resultObj = runtime.executeObjectScript(""
				+ "mortgageCalculations."
				+ service + "."
				+ calculation
				+ "("
				+ gson.toJson(params)
				+ ")");
		return resultObj;
	}

	/**
	 * Wrapper around doCalculation to provide a JsonObject result instead of Json
	 * @param service - name of service
	 * @param calculation - name of calculation
	 * @param params - JSON parameters object
	 * @return - JSONObject
	 */
	public JsonObject doCalculationAsJsonObject(String service, String calculation, JsonObject params) {
		V8Object resultObj = doCalculation(service, calculation, params);
		V8Object json = runtime.getObject("JSON");
		V8Array parameters = new V8Array(runtime).push(resultObj);
		String result = json.executeStringFunction("stringify", parameters);
		resultObj.release();
		parameters.release();
		json.release();
		//parameters.release();
		JsonParser parser = new JsonParser();
		JsonObject jsObj = parser.parse(result).getAsJsonObject();
		return jsObj;
	}

	private String readFileFromClasspath(final String fileName) throws IOException, URISyntaxException {
		return new String(Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource(fileName).toURI())));
	}
}
